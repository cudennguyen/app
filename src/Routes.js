import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { Router, Scene, Stack } from 'react-native-router-flux';
import { Root } from "native-base";
import QRScaner from '../src/pages/QRScaner';
import Login from '../src/pages/Login';
import SearchValue from '../src/pages/SearchValue';
import Main from '../src/pages/Main';
import History from '../src/pages/History';
import HistoryDetail from './pages/HistoryDetail';
export default SrcRoute = class Routes extends Component {
    render() {
        return (
            <Root>
                <Router>
                    <Stack key="root" hideNavBar={true}>
                        <Scene key="qrscaner" component={QRScaner} />
                        <Scene key="login" component={Login} initial={true}/>
                        <Scene key="searchValue" component={SearchValue}  />
                        <Scene key="main" component={Main} />
                        <Scene key="history" component={History} />
                        <Scene key="historyDetail" component={HistoryDetail} />
                    </Stack>
                </Router>
            </Root>
        )
    }
}
