import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity, Dimensions, StatusBar, BackHandler, Alert, FlatList
} from 'react-native';
import { Text, Icon, Content, Container } from 'native-base';
import UtilAsyncStorage from '../../src/util/AsyncStorage';
import Configs from '../../src/configs/index';
import { connect } from 'react-redux';
const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;

class HistoryDetail extends Component {
    constructor(props) {
        super();
        this.state = {
            data: [],
            child:{}
        }
    }
    componentDidMount() {
        console.log(Configs.hostname + '/parent/attendances/view/' + this.props.attending_id);
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/parent/attendances/view/' + this.props.attending_id,
            {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + this.props.token
                }
            }
        ).then((responseJson) => {
            console.log(responseJson);
            this.setState({
                data : responseJson,
                child : responseJson.child
            })
        })
    }
    render() {
        return (
            <Container>
                <View style={Style.header}>
                    <View style={Style.headerLeft}>
                        <TouchableOpacity>
                            <Icon name="close" size={25} style={Style.iconClose} />
                        </TouchableOpacity>
                         <Text style={{ fontSize: 25, fontWeight: 'bold', marginLeft: 15, paddingBottom: 20 }}>{this.state.child.first_name} {this.state.child.last_name}</Text>
                    </View>
                </View>
                <Content>
                    <View style= {{backgroundColor:'blue'}}>
                        <View style={{ width: widthW, height: 65, borderBottomWidth: 1, borderTopWidth: 1, alignItems: 'center', flexDirection: 'row' }}>
                            <View style={{ flexDirection: 'column', marginLeft: 5 }}>
                                <View style={{ flexDirection: 'row' }}>
                                </View>
                            </View>
                        </View>
                    </View>
                </Content>
            </Container>

        );
    }
}

const Style = StyleSheet.create({
    iconClose: {
        fontSize: 35,
        marginLeft: 15,
        color: '#BBBBBB'
    },
    iconCalendar: {
        fontSize: 40,
        paddingRight: 10,
        color: '#BBBBBB'
    },
    iconHistory: {
        fontSize: 30,
        paddingLeft: 10,
        color: '#BBBBBB'
    },
    header: {
        flexDirection: 'row',
        width: widthW,
        height: 80,
        shadowColor: "red",
        borderBottomWidth: 1,
        borderBottomColor: '#BBBBBB',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    headerLeft: {
        flexDirection: 'column',
        width: '100%',
    }
})
const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token
    };
}
export default HistoryDetail = connect(mapStateToProps, null)(HistoryDetail)
