import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity, Dimensions, StatusBar, BackHandler, Alert, FlatList
} from 'react-native';
import { Text, Icon, Content, Container } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Configs from '../../src/configs/index';
import { connect } from 'react-redux';
import UtilAsyncStorage from '../../src/util/AsyncStorage';
import ProgressBar from 'react-native-progress/Bar';
import { strings } from '../../src/i18n';
const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;

class Main extends Component {
    constructor(props) {
        super();
        this.state = {
            notifications: {},
            checkin: {
                num_attendance_dates: 0,
                total_date: 0
            },
            percent_checkin: 1
        }
        this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
            BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
    }
    componentDidMount() {
        this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
            BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
        );
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/attender/notifications',
            {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + this.props.token
                }
            }
        ).then((responseJson) => {
            if (responseJson.status == true) {
                this.setState({
                    notifications: responseJson.data
                })
                
            }
        });
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/attender/info ',
            {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + this.props.token
                }
            }
        ).then((responseJson) => {
            this.setState({
                checkin: responseJson.data,
                percent_checkin: parseInt(responseJson.data.num_attendance_dates) / parseInt(responseJson.data.total_date)
            })
        })
    }
    onBackButtonPressAndroid = () => {
        Alert.alert(
            strings("messageError.requestExitApp1"),
            strings("messageError.requestExitApp2"), [{
                text: strings("common.cancel"),
                style: 'cancel'
            }, {
                text: strings("common.ok"),
                onPress: () => BackHandler.exitApp()
            },], {
            cancelable: false
        }
        )
        return true;
    };
    logout() {
        this.props.onlogout();
        Actions.login();
    }
    render() {
        return (
            <Container>
                <StatusBar hidden={false} />
                <View style={Style.header}>
                    <View style={{ marginLeft: 10, flexDirection: 'column', width: widthW - 40 }} >
                <Text style={{ fontSize: 23, fontWeight: 'bold' }}>Chào buổi sáng, {this.props.user? this.props.user.name : ''}!</Text>
                        <View style={{ marginLeft: 10, flexDirection: 'row' }}>
                            <TouchableOpacity style={{ width: 100, height: 30, flexDirection: 'row', alignItems: 'center' }}>
                                <Icon name="ios-contact" style={Style.headerButtonIcon} />
                                <Text style={Style.headerButtonText}>Học bạ</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={Style.headerButton}>
                                <Icon name="ios-mail" style={Style.headerButtonIcon} />
                                <Text style={Style.headerButtonText}>Tin nhắn</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ marginBottom: 5, justifyContent: 'flex-end' }}>
                        <TouchableOpacity onPress={() => this.logout()}>
                            <Icon name="settings" style={{ color: "#4D4D4D", fontSize: 17 }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <Content>
                    <View style={Style.historyView}>
                        <View style={Style.historyViewContent}>
                            <Text style={{ fontSize: 13, color: '#E3D1C3' }}>Tổng số ngày đi học trong tháng</Text>
                            <Text style={{ fontSize: 25, color: '#E3D1C3' }}>{this.state.checkin.num_attendance_dates}/<Text style={{ fontSize: 15, color: '#50534E' }}>{this.state.checkin.total_date}</Text></Text>
                            <ProgressBar progress={this.state.percent_checkin} width={150} height={15} color={'#E3D1C3'} unfilledColor={'#4D4D4D'} borderWidth={0} borderRadius={10} />
                        </View>
                        <View style={Style.historyViewButton}>
                            <TouchableOpacity style={Style.historyButton} onPress={() => Actions.history()}>
                                <Text style={{ color: '#E3D1C3' }}>Lịch sử</Text>
                            </TouchableOpacity>
                            <View style={{ width: '65%' }}>
                                <TouchableOpacity style={Style.historyQRButton} onPress={() => Actions.qrscaner()}>
                                    <Text style={{ color: '#7D7570' }}>Quét QR</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                    <FlatList
                        data={this.state.notifications}
                        renderItem={({ item }) => (
                            <View style={Style.infoView}>
                            <View style={Style.infoViewContent}>
                                <Text style={{ fontSize: 13, color: '#7D7570' }}>THÔNG TIN CHUNG</Text>
                                <Text style={{ fontSize: 13, fontWeight: 'bold' }}>{item.title}</Text>
                                <Text style={{ fontSize: 13 }} numberOfLines={2}>{item.content}</Text>
                            </View>
                            <View style={Style.infoViewButton}>
                                <TouchableOpacity style={Style.infoButton}>
                                    <Text style={{ color: '#7D7570' }}>Xem thêm</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        )}
                        //Setting the number of column
                        numColumns={3}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </Content>
            </Container>

        );
    }
}

const Style = StyleSheet.create({
    headerButton: {
        width: 100,
        height: 30,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerButtonText: {
        color: '#4D4D4D',
        fontSize: 13,
        marginLeft: 5
    },
    headerButtonIcon: {
        fontSize: 25,
        justifyContent: 'center',
        color: '#4D4D4D',
    },
    header: {
        backgroundColor: Configs.headerColor,
        flexDirection: 'row',
        width: widthW,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    historyButton: {
        width: '35%',
        height: 40,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#E3D1C3',
        justifyContent: 'center',
        alignItems: 'center'
    },
    historyView: {
        marginTop: 2,
        width: widthW,
        height: 150,
        backgroundColor: '#577A75',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.7,
        shadowRadius: 2.22,
        elevation: 3,
    },
    historyViewContent: {
        marginLeft: 15,
        marginTop: 10,
        height: 90,
    },
    historyViewButton: {
        marginLeft: 15,
        flexDirection: 'row',
        width: widthW,
    },
    historyQRButton: {
        width: '53%',
        height: 40,
        borderRadius: 4,
        backgroundColor: '#F3C963',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: 25
    },
    infoViewButton: {
        marginLeft: 15,
        flexDirection: 'row',
        width: widthW,
    },
    infoView: {
        marginTop: 5,
        width: widthW,
        height: 150,
        backgroundColor: '#E3D1C3',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    infoButton: {
        width: '35%',
        height: 40,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#7D7570',
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoViewContent: {
        marginLeft: 15,
        marginTop: 10,
        height: 90,
    },
})
const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token,
        user: state.auth.user,
        isLogin: state.auth.isLogin
    };
}
const mapDispatchToProps = (dispatch) => {
    return {
        getAsyncStorageLanguage: () => {
            dispatch(ActionsLanguage.getAsyncStorage());
        },
        onlogout: () => {
            dispatch(ActionsAuthAuth.logout());
        },
    }
}
export default Main = connect(mapStateToProps, mapDispatchToProps)(Main)
