import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity, Dimensions, StatusBar, BackHandler, Alert, FlatList
} from 'react-native';
import { Text, Icon, Content, Container } from 'native-base';
import UtilAsyncStorage from '../../src/util/AsyncStorage';
import Configs from '../../src/configs/index';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var year = new Date().getFullYear(); 
var month = new Date().getMonth() + 1;
month = monthNames[month];
class History extends Component {
    constructor(props) {
        super();
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        UtilAsyncStorage.fetchAPI(
            Configs.hostname + '/attender/attendances/history/' + this.props.user.childs[0].id,
            {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + this.props.token
                }
            }
        ).then((responseJson) => {
            if (responseJson.status == true) {
                this.setState({
                    data : responseJson.data.attendances
                 })
                 
            }
        })
    }
    render() {
        return (
            <Container>
                <View style={Style.header}>
                    <View style={Style.headerLeft}>
                        <TouchableOpacity onPress={() => Actions.pop()}>
                            <Icon name="close" size={25} style={Style.iconClose} />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', marginLeft: 15, paddingBottom: 20 }}>Lịch sử</Text>
                    </View>
                    <View style={Style.headerRight}>
                        <Icon name="ios-calendar" size={25} style={Style.iconCalendar} />
                        <Text style={{ fontSize: 15, paddingRight: 15, color:'#BBBBBB' }}>Chọn tháng</Text>
                    </View>
                </View>
                <View style={{height:50, flexDirection:'row', width:widthW, backgroundColor:'#DDDDDD', alignItems:'center'}}>
                    <Text style={{ width: '70%', marginLeft:10, fontWeight:'bold'}}>{month} {year}</Text>
                    <Text style={{ width: '30%', justifyContent: 'flex-end', alignItems: 'flex-end', fontSize:15, fontWeight:'bold'}}>{this.state.data.length} buổi</Text>
                </View>
                <Content>
                <FlatList
                        style={{marginTop:2}}
                        data={this.state.data}
                        renderItem={({ item }) => (
                            <View style={{width:widthW, height:65, borderBottomWidth:1, borderTopWidth:1, alignItems:'center', flexDirection:'row'}}>
                                <Icon name={item.attending_status == 1 ? "ios-log-in" : "ios-log-out" } size={40} style={Style.iconHistory} />
                                <View style={{flexDirection:'column', marginLeft:5}}>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style= {{fontSize:12,width:'40%'}}>{item.first_name} {item.last_name}</Text>
                                        <Text style={{marginLeft:25, fontSize:12, width:'20%'}}>{item.attending_date}</Text>
                                        <Text style={{marginLeft:25, fontSize:12, width:'20%'}}>{item.attending_status == 1 ? item.attending_time_begin : item.attending_time_end }</Text>
                                    </View>
                                    <Text style= {{fontSize:12}}>{item.school_address}</Text>
                                </View>
                            </View>
                        )}
                        //Setting the number of column
                        numColumns={3}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </Content>
            </Container>

        );
    }
}

const Style = StyleSheet.create({
    iconClose: {
        fontSize: 35,
        marginLeft: 15,
        color:'#BBBBBB'
    },
    iconCalendar: {
        fontSize: 40,
        paddingRight: 10,
        color:'#BBBBBB'
    },
    iconHistory: {
        fontSize: 30,
        paddingLeft: 10,
        color:'#BBBBBB'
    },
    header: {
        flexDirection: 'row',
        width: widthW,
        height: 80,
        shadowColor: "red",
        borderBottomWidth: 1,
        borderBottomColor: '#BBBBBB',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    headerRight: {
        flexDirection: 'row',
        width: '60%',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        paddingBottom: 10
    },
    headerLeft: {
        flexDirection: 'column',
        width: '40%',
    }
})
const mapStateToProps = (state, ownProps) => {
    return {
        token: state.auth.token,
        user: state.auth.user
    };
}
export default History = connect(mapStateToProps, null)(History)
