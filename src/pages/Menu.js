import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
} from 'react-native';

import { Header, Thumbnail, Button, Right, List, ListItem, Left } from 'native-base'
import Configs from '../../src/configs/index';
import { Actions } from 'react-native-router-flux'
import { Content, Icon } from "native-base";
import {strings} from '../../src/i18n';
const heightW = Dimensions.get('window').height;
class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false
    };
  }
  myPackage = () => {
    Actions.MyPackage({ titlePackage: strings("package.myPackage") })
  }
  onPressMenu = (route) => {
    switch (route) {  
      case "logout": this.props.closeDrawer(); Actions.login(); break;
      case "setting": this.props.closeDrawer(); Actions.setting(); break;
    }
  }
  render() {
    const { user, language } = this.props;
    const datas = [
      {
        name: strings('menu.profile'),
        route: "profile",
        icon: "ios-person",
        bg: "#C5F442",
        isLogin: true
      },
      {
        name: strings('menu.setting'),
        route: "setting",
        icon: "ios-settings",
        bg: "#C5F442",
        isLogin: true
      },    
      {
        name: strings('menu.logout'),
        route: "logout",
        icon: "md-exit",
        bg: "#C5F442",
        isLogin: true
      },    
      
    ];
    return (
      <View style={{ flex: 1 , backgroundColor : Configs.iconColor  }}>       
        <Content bounces={false} style={styles.content}>
          <List>
            {datas.map((item, index) => (           
                <ListItem iconLeft style={[{ borderBottomColor:'white' ,borderBottomWidth:1,   
                 marginLeft:0 ,alignItems:'center', height: heightW/7},index==0?{borderTopWidth:1,borderTopColor:'white' ,}:null]}
                  onPress={() => this.onPressMenu(item.route)}
                  key={index}
                >
                <View style={{flexDirection:'column',width:'100%',height:'100%', alignItems:'center', left:'10%'}}>              
                  <Icon                                      
                    name={item.icon}
                    style={styles.iconButton}

                  />     
                  <Text style={{color:'white', fontSize:10}}>{item.name}</Text>      
                  </View>    
                </ListItem>
             
            ))}
          </List>
        </Content>
      </View>
    );

  }
}


const styles = StyleSheet.create({
  content: {
    backgroundColor: Configs.headerColor
  },
  iconButton: {
    color: 'white',   
    fontSize: 40
  },
});
export default Menu
