/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Image, StyleSheet, View, Dimensions, Alert, Modal, Text, FlatList, TouchableOpacity, Icon } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { Container } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Configs from '../../src/configs/index';
import UtilShowMessage from '../../src/util/ToastShow';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;
class QRScaner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reload: true,
      status: false,
      data: {}
    };

  }
  onHandleBack = () => {
    this.scanner.reactivate();
  }
  onSuccess(e) {
    console.log(this.props.user.childs);
    if (this.props.user.childs.length > 1) {
      this.setState({
        status: true,
        data: e.data
      })
    }
    else {
      if (this.props.user.childs[0]) {
        this.checkin(this.props.user.childs[0].id)
        this.setState({
          data: e.data
        })
      }
    }
    // Actions.searchValue({ data: e.data, onHandleBack: () => this.onHandleBack()  })

  }
  checkin(id) {
    this.setState({
      status: false
    })
    fetch(Configs.hostname + '/attender/checkin', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.props.token
      },
      body: JSON.stringify({
        child_id: id,
        qr_code: this.state.data
      })
    }).then((responseJson) => {
      response = JSON.parse(responseJson._bodyInit);
      console.log(response);
      if (response.status == true) {
        UtilShowMessage.ToastShow("Checkin thành công");
        Actions.pop();
      }
      else {
        UtilShowMessage.ToastShow("Checkin thất bại");
        this.scanner.reactivate();
      }
    })
  }
  componentWillReceiveProps() {
    this.scanner.reactivate();
  }
  render() {
    return (
      <Container>
        <View style={{ alignItems: 'center' }}>
          <QRCodeScanner
            containerStyle={{
              paddingBottom: 100
            }} cameraStyle={{
              height: Dimensions.get('window').height,
            }}
            style={{ flex: 1 }}
            ref={(node) => { this.scanner = node }}
            onRead={this.onSuccess.bind(this)}
          />
          
          <Image style={{ position: 'absolute', marginTop: Dimensions.get('window').height / 5, height: Dimensions.get('window').height / 2, width: Dimensions.get('window').height / 2 }} source={require('../../src/public/images/QR.png')} />
        </View>
        <Dialog
          visible={this.state.status}
          onTouchOutside={() => {
            this.scanner.reactivate();
            this.setState({ status: false });
          }}
        >
          <DialogContent>
            <View style={{ width: 250 }}>
              <Text>Vui lòng chọn học sinh để check in</Text>
              <FlatList
                style={{ marginTop: 2 }}
                data={this.props.user.childs}
                renderItem={({ item }) => (
                  <View>
                    <TouchableOpacity style={{ width: widthW, height: 25, borderBottomWidth: 1, alignItems: 'center', flexDirection: 'row' }} onPress={() => this.checkin(item.id)}>
                      <View style={{ flexDirection: 'column', marginLeft: 5 }}>
                        <View style={{ flexDirection: 'row' }}>
                          <Text style={{ fontSize: 12 }}>{item.first_name} {item.last_name}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: widthW, height: 25, borderBottomWidth: 1, alignItems: 'center', flexDirection: 'row' }} onPress={() => this.checkin(item.id)}>
                      <View style={{ flexDirection: 'column', marginLeft: 5 }}>
                        <View style={{ flexDirection: 'row' }}>
                          <Text style={{ fontSize: 12 }}>{item.first_name} {item.last_name}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                )}
                //Setting the number of column
                numColumns={3}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>

          </DialogContent>
        </Dialog>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    paddingTop: 30
  },
});
const mapStateToProps = (state, ownProps) => {
  return {
    token: state.auth.token,
    user: state.auth.user
  };
}
export default QRScaner = connect(mapStateToProps, null)(QRScaner)