/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import Configs from '../../src/configs/index';
import {
  Platform,
  Image,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Linking,
  Alert,
  BackHandler,
  FlatList,
  Dimensions,
  ImageBackground,RefreshControl
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Right,
  Icon,
  Thumbnail,
  Switch,
  Textarea,
  Button,
  Picker
} from 'native-base';
import Header from '../../src/components/Header';
import UtilAsyncStorage from '../../src/util/AsyncStorage';
import UtilShowMessage from '../../src/util/ToastShow';
import { connect } from 'react-redux';
import { strings } from '../../src/i18n';
const heightW = Dimensions.get('window').height;
const widthW = Dimensions.get('window').width;
class SearchValue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      empData: [],
      selected: 0,
      area:"",
      imgLink:"",
      refreshing: false
    };
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }
  onChangeState(isActive) {
    this.setState({
      Device_Status: isActive,
      data: {}
    })
  }
  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );   
    UtilAsyncStorage.fetchAPI(
      Configs.hostname + '/api/deviceInfo?deviceId='+this.props.data,
      {
        method: 'GET',
        headers:
          {
            'Authorization': this.props.token
          },
      }
    ).then((responseJson) => {
      if (responseJson[0].isDeivce==true) {
        this.setState({
          data: responseJson[0],
          empData: 
          {
            deviceEmpNo: responseJson[0].deviceEmpNo,
            deviceEmpName:responseJson[0].deviceEmpName,
            deviceTeam:responseJson[0].deviceTeam
          },
          selected: responseJson[0].deviceStatus,
          area: responseJson[0].deviceMemo,
          imgLink: Configs.hostname+'/upload/'+ responseJson[0].deviceEmpNo+'.jpg'
        })
      }
      else {
        UtilShowMessage.ToastShow("Không tồn tại dữ liệu của thiết bị");
      }
    })
  }
  onValueChange(value) {
    this.setState({
      selected: value
    });
  }
  onSelectNewEmp = (value) => {
    this.setState({
      empData: {
        deviceEmpNo: value.EmpNo,
        deviceEmpName: value.EmpName,
        deviceTeam: value.Team,
        empEmail: value.EmpEmail,   
      },
      imgLink:Configs.hostname+'/upload/'+ value.EmpNo+'.jpg'
    })
  }
  updateInforDevice = () => {
    var details = {
      'deviceId': this.state.data.deviceId,
      'empNo': this.state.empData.deviceEmpNo,
      'deviceStatus': this.state.selected,
      "deviceMemo": this.state.area
    };
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");


    UtilAsyncStorage.fetchAPI(
      Configs.hostname + '/api/deviceEdit',
      {
        method: 'POST',
        headers:
          {
            'Content-Type': "application/x-www-form-urlencoded",
            'Authorization': this.props.token
          },
        body: formBody
      }
    ).then((responseJson) => {
      if (responseJson) {
        UtilShowMessage.ToastShow(strings('message.editSuccess'))
      }

    })
  }
  onBackButtonPressAndroid = () => {
    if (this.props.onHandleBack) {
      this.props.onHandleBack();
    }
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this.componentDidMount();
    setInterval(() => {
        this.setState({ refreshing: false });
    }, 1000);
}
  render() {
    return (
      <ImageBackground source={Configs.background}  style={Style.Image} >
        <Header title={this.state.data.deviceModel}
          onHandleBack={this.props.onHandleBack ? this.props.onHandleBack : null}
        />
        <Content
          refreshControl={
            <RefreshControl
            style={{backgroundColor: '#E0FFFF'}}
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
            tintColor="#ff0000"
            colors={['#ff0000', '#00ff00', '#0000ff']}
            progressBackgroundColor="#F9F9FA"
            />}>    
          <TouchableOpacity style={Style.View_Avatar} onPress={() => Actions.employee({ data: this.state.empData, onSelectNewEmp: this.onSelectNewEmp })}>
            <View style={{ width: '40%' }}>
              <Image style={Style.avatar} 
                                source={
                                { uri: this.state.imgLink}
                                } onError={ ()=>this.setState({imgLink:Configs.hostname+'/upload/No-img.jpg'})}/>                            
            </View>
            <View style={{ width: '60%' }}>
              <Text numberOfLines={1} style={[Style.TextName, { fontWeight: 'bold', fontStyle: 'italic', fontSize:20, color:Configs.headerColor }]}>{this.state.empData.deviceEmpName}</Text>
              <View style={{ flexDirection: 'row' }}>
                <Text>{strings('infor.team')}: </Text>
                <Text numberOfLines={1} style={Style.TextName}>{this.state.empData.deviceTeam ? this.state.empData.deviceTeam : null}</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={Style.View_Detail}>
            <Card style={Style.Card}>             
              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View >
                      <Text style={Style.textType}>{strings('infor.deviceNumber')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={Style.TextDetail}>
                      {this.state.data.deviceId}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View >
                      <Text style={Style.textType}>{strings('infor.maker')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={Style.TextDetail}>
                      {this.state.data.deviceMaker}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View >
                      <Text style={Style.textType}>{strings('infor.model')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={Style.TextDetail}>
                      {this.state.data.deviceModel}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View >
                      <Text style={Style.textType}>{strings('infor.serial')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={Style.TextDetail}>
                      {this.state.data.deviceSeri}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View>
                      <Text style={Style.textType}>{strings('infor.dateUpdate')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={Style.TextDetail}>
                      {this.state.data.deviceDateBuy}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View >
                      <Text style={Style.textType}>{strings('infor.type')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={Style.TextDetail}>
                      {this.state.data.deviceType}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View>
                      <Text style={Style.textType}>{strings('infor.OS')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={Style.TextDetail}>
                      {this.state.data.deviceOs}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>

                    <View>
                      <Text style={Style.textType}>{strings('infor.CPU')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={[Style.TextDetail, { marginTop: -8 }]}>
                      {this.state.data.deviceCpu}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View>
                      <Text style={Style.textType}>{strings('infor.memory')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={Style.TextDetail}>
                      {this.state.data.deviceMemory}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View >
                      <Text style={Style.textType}>{strings('infor.HDD')}</Text>
                    </View>
                  </View>
                  <View style={Style.View_Content}>
                    <Text style={Style.TextDetail}>
                      {this.state.data.deviceHdd}
                    </Text>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <View style={Style.View_Card}>
                    <View >
                      <Text style={Style.textType}>{strings('infor.status')}</Text>
                    </View>
                  </View>
                  <View style={[Style.View_Content, { flexDirection: 'row' }]}>
                    <Picker
                      note
                      mode="dropdown"
                      style={{ width: '80%' }}
                      selectedValue={this.state.selected}
                      onValueChange={this.onValueChange.bind(this)}
                    >
                      <Picker.Item label={strings('select.archiving')} value={0} />
                      <Picker.Item label={strings('select.using')} value={1} />
                      <Picker.Item label={strings('select.repairing')} value={2} />
                    </Picker>
                  </View>
                </Body>
              </CardItem>

              <CardItem style={Style.CardItem}>
                <Body style={Style.Body}>
                  <Textarea rowSpan={5} style={{ width: '100%' }} bordered placeholder={strings('infor.memo')}   onChangeText={(area) => this.setState({ area })}  value={this.state.area}/>
                </Body>
              </CardItem>
            </Card>
            <View style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <Button style={{
                width: 100, flex: 1,
                justifyContent: 'center',
                backgroundColor:Configs.headerColor,
                alignItems: 'center', marginBottom: 10
              }}
                onPress={() => this.updateInforDevice()}
              >
                <Text style={{color:'white'}}>{strings('common.update')}</Text>
              </Button>
            </View>
          </View>

        </Content>
      </ImageBackground >
    );
  }
}
const Style = StyleSheet.create({
  Card: {
    width: '96%',
  },
  textType: {
    fontWeight: 'bold',
    fontStyle: 'italic',
    color:Configs.headerColor
  },
  CardItem: {
    height: 'auto',
    backgroundColor: 'rgb(255,255,255)',
  },
  Image: {
    height: heightW,
    width: widthW,
    flex: 1,
    backgroundColor: '#F2F2F2',
  },
  TextName: {
    color: '#000000',
    borderColor: 'rgb(206,187,53)',
    height: 40,
  },
  Text: {
    color: '#FFFFFF',
    width: 250,
  },
  TextDetail: {
    color: '#000000',
    marginLeft: 20,
    marginTop: 10,
    width: 200,
  },
  avatar: {
    marginBottom: 10,
    width: 120,
    height: 120,
    borderRadius: 120 / 2,
  },
  View_Avatar: {
    paddingTop: 20,
    marginLeft: 15,
    flexDirection: 'row'
  },
  View_Detail: {
    alignItems: 'center',
  },
  Icon: {
    color: Configs.iconColor,
    fontSize: 38,
    width: 50,
  },
  Body: {
    flexDirection: 'row',
  },
  View_Card: {
    width: '20%',
    height: '100%',
    marginRight: 20
  },
  View_Content: {
    height: '100%',
    borderLeftWidth: 1,
    borderColor: '#0066B3',
  }
})
const mapStateToProps = (state, ownProps) => {
  return {
    language: state.Language.language,
    token: state.auth.Token

  };
}
export default SearchValue = connect(mapStateToProps, null)(SearchValue)
