import ReactNative, { AsyncStorage } from 'react-native';
import I18n from 'react-native-i18n';

// Import all locales
import en from './locales/en.json';
import ja from './locales/ja.json';
import vi from './locales/vi.json';
// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

// Define the supported translations
I18n.translations = {
  en,
  ja,
  vi,
};
I18n.languageCountry = {
  en: 'US',
  ja: 'JP',
  vi: 'VN',
};
I18n.languageName = {
  en: 'English',
  ja: '日本',
  vi: 'Việt Nam',
};
// The method we'll use instead of a regular string
export function strings(name, params = {}) {
  return I18n.t(name, params);
};
export function changeLanguage(language) {
  I18n.locale(language)
}
export function setLanguage(language) {
  AsyncStorage.setItem('language', JSON.stringify(language))
}
export default I18n;