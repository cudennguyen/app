const defaultState = {
    language: 'en',
    statusLanguage: false
};
import {setLanguage} from '../../i18n';
export default function ReducersLanguageLanguage(state = defaultState, action) {
    if (action.language)
        action.language = action.language.substring(0, 2)
    switch (action.type) {
        case 'LANGUAGE':
            setLanguage(action.language);
            return Object.assign({}, state, {
                language: action.language,
                statusLanguage : true
            });
        default:
            return state;
    }
}