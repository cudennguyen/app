const defaultState = {
    isLogin: false,
    EmpEmail: '',
    Message: '',
    Position: '',
    EmpNo: '',
    Team: '',
    EmpName: '',
    Token: ''
};

export default function ReducersAuthAuth(state = defaultState, action) {
    switch (action.type) {
        case 'LOGIN':
        return Object.assign({}, state, {
            isLogin: true,
            message: action.user.message,
            token: action.user.data.token,
            user: action.user.data.user,
        });
        case 'LOGOUT':
            return Object.assign({}, state, {
                isLogin: false,
                message: '',
                token: '',
                user: null
            });
        default:
            return state;
    }
}