import UtilAsyncStorage from '../../../src/util/AsyncStorage';
import I18n from 'react-native-i18n';
export default ActionsLanguage = {
    language: (language) => {     
        return {
            type: 'LANGUAGE',
            language: language
        }
    },
    getAsyncStorage: () => {       
        return (dispatch) => {
            UtilAsyncStorage.getStorageLanguage().then(language => {
                const currentLocale = I18n.currentLocale();
                languageSet = language == null ? currentLocale.slice(0, 2) : language;
                I18n.locale = languageSet;
                dispatch(ActionsLanguage.language(languageSet));
            });
        }
    }
}
