import UtilAsyncStorage from '../../../src/util/AsyncStorage';
import {AsyncStorage} from 'react-native';
export default ActionsAuthAuth = {
    login: (user) => {
        UtilAsyncStorage.setLogin(user);
        return {
            type: 'LOGIN',
            user: user
        };
    },
    logout: () => {
        UtilAsyncStorage.setLogout();
        return {
            type: 'LOGOUT'
        };
    },
    asyncStorage: (user) => {
        return {
            type: 'ASYNC_STORAGE',
            user: user
        }
    },
    getAsyncStorage: () => {
        return (dispatch) => {
            UtilAsyncStorage.getStorageUser().then(data => {
                dispatch(ActionsAuthAuth.asyncStorage(data));
            });
        }
    }
}
