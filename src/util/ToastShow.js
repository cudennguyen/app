import React, { Component } from 'react';
import { Toast } from 'native-base';
const UtilShowMessage = {
	ToastShow: (message, type, position) => {
		Toast.show({ text: message ? message.toString() : 'Error!', type: type, position: position , duration: 10000, buttonText: 'Okay' })
	}
}
export default UtilShowMessage		