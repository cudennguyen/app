import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class Error extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (         
                <Text style={{ color: '#ff4d4d',textAlign:'left' }}>{this.props.value}</Text>   
        )
    }
}