import React, { Component } from 'react';
import { Icon, Left,Header, Right, Button, Title, Body } from 'native-base';
import {
    StyleSheet,
    Dimensions,
    View,
    TouchableOpacity,
    Text,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Configs from '../../src/configs/index';
import { strings } from '../../src/i18n';
const { height } = Dimensions.get('window');

class HeaderComponent extends React.PureComponent {
    handleBackButton = () => {
        if (this.props.onHandleBack) {
          this.props.onHandleBack();    
        }
      }
    onBack = () => {
        if (this.props.onHandleBack) {
          this.props.onHandleBack();
          Actions.pop()
        }
        else {
          Actions.pop()
        }
      }
    render() {
        return (
            <Header style={{ backgroundColor: Configs.headerColor, }}>
                <Left style={{ marginLeft: 10 }}>
                    <TouchableOpacity onPress={this.onBack} >
                        <Icon name="ios-arrow-round-back"
                            style={{ color: '#FFFFFF', fontSize: 38 }} />
                    </TouchableOpacity>
                </Left>
                <Body style={{ marginRight: 100 }}>
                    <Text style={[Style.Text, { fontSize: 15 }]}>{this.props.title}</Text>
                </Body>
                <Right style={{ marginRight: 10 }}>
                </Right>
            </Header>
        );
    }
}

const Style = StyleSheet.create({ 
      Text: {
        color: '#FFFFFF',
        width: 250,
      },     
});

export default HeaderComponent;